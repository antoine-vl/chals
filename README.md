# Chals

* Find the challenges in the public folders.
* Solutions/Work are also accessible.

## Challenges

#### Chal1

We collected suspicious HTTP traffic in `chal1.pcap`. What's been exfiltrated?

#### Chal2

The user also sent a weird email externally containing two files:

* `Note.txt`
* `flower.jpg`

Can you find what's going on here?


