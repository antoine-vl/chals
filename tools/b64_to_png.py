import sys
import base64


if len(sys.argv) != 3:
    print("Usage: b64_to_png.py <base64 encoded string> <output file>")

with open(sys.argv[2],'w') as f:
    f.write(base64.b64decode(sys.argv[1]))

print("File "+sys.argv[2]+" created.")
    
