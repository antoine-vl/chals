from scapy.all import *
import base64

pcap = rdpcap('chal1.pcap')

pcap.show()

i = 0
l = []
for packet in pcap:
    e = packet[Raw].load.decode('utf-8').split('\n')[0]
    l.append(e)
s=""
for p in l:
    s += p.split('payload=')[1].split()[0]

with open('flag.png','wb') as f:
    f.write(base64.b64decode(s))

